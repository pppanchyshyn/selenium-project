package com.epam.ta10.selenium.project.bo;

import com.epam.ta10.selenium.project.model.User;
import com.epam.ta10.selenium.project.po.GmailLoginPage;
import com.epam.ta10.selenium.project.po.GmailPasswordPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GmailAuthenticationBO {

  private GmailLoginPage gmailLoginPage;
  private GmailPasswordPage gmailPasswordPage;
  private static Logger logger = LogManager.getLogger(GmailAuthenticationBO.class);

  public GmailAuthenticationBO() {
    gmailLoginPage = new GmailLoginPage();
    gmailPasswordPage = new GmailPasswordPage();
  }

  public void login(User user) {
    enterLoginAndPressButtonNext(user.getLogin());
    enterPasswordAndPressButtonNext(user.getPassword());
  }

  private void enterLoginAndPressButtonNext(String login) {
    logger.info("Entering user login...");
    gmailLoginPage.getLogin().clearAndType(login);
    logger.info("Pressing button next..");
    gmailLoginPage.getNext().clickAfterButtonBecomeClickable();
  }

  private void enterPasswordAndPressButtonNext(String password) {
    logger.info("Entering user password...");
    gmailPasswordPage.getPassword().clearAndType(password);
    logger.info("Pressing button next...");
    gmailPasswordPage.getNext().clickAfterButtonBecomeClickable();
  }
}
