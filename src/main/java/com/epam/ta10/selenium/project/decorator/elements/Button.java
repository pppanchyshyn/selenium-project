package com.epam.ta10.selenium.project.decorator.elements;

import com.epam.ta10.selenium.project.utils.DriverProvider;
import com.epam.ta10.selenium.project.utils.WaiterProvider;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class Button extends Element {

  public Button(WebElement webElement) {
    super(webElement);
  }

  public void click() {
    webElement.click();
  }

  public void clickAfterButtonBecomeClickable() {
    WaiterProvider.waitElementToBeClickable(webElement).click();
  }

  public void clickWithJS() {
    JavascriptExecutor executor = (JavascriptExecutor) DriverProvider.getDriver();
    executor.executeScript("arguments[0].click();", webElement);
  }
}
