package com.epam.ta10.selenium.project.po;

import com.epam.ta10.selenium.project.decorator.CustomFieldDecorator;
import com.epam.ta10.selenium.project.utils.DriverProvider;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractPage {

  AbstractPage() {
    PageFactory.initElements(new CustomFieldDecorator(DriverProvider.getDriver()), this);
  }
}
