package com.epam.ta10.selenium.project.utils;

import com.epam.ta10.selenium.project.decorator.elements.Element;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaiterProvider {

  private final static int TIMEOUT_IN_SECONDS = 120;
  private final static int SLEEP_IN_MILLISECONDS = 2000;

  private WaiterProvider() {
  }

  public static WebElement waitElementToBeClickable(WebElement element) {
    return new WebDriverWait(DriverProvider.getDriver(), TIMEOUT_IN_SECONDS, SLEEP_IN_MILLISECONDS)
        .ignoring(StaleElementReferenceException.class, ElementNotInteractableException.class)
        .until(ExpectedConditions.elementToBeClickable(element));
  }

  public static WebElement waitElementToBeClickable(Element element) {
    return new WebDriverWait(DriverProvider.getDriver(), TIMEOUT_IN_SECONDS, SLEEP_IN_MILLISECONDS)
        .ignoring(StaleElementReferenceException.class, ElementNotInteractableException.class)
        .until(ExpectedConditions.elementToBeClickable(element.getWebElement()));
  }
}
