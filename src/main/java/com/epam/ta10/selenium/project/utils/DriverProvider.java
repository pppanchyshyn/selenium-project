package com.epam.ta10.selenium.project.utils;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverProvider {

  private final static int TIMEOUT = 120;
  private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

  private DriverProvider() {
  }

  public static WebDriver getDriver() {
    if (Objects.isNull(driver.get())) {
      driver.set(new ChromeDriver());
      driver.get().manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
      driver.get().manage().window().maximize();
    }
    return driver.get();
  }

  public static void close() {
    if (!Objects.isNull(driver)) {
      driver.get().quit();
      driver.remove();
    }
  }
}
