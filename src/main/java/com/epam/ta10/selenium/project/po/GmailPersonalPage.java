package com.epam.ta10.selenium.project.po;

import com.epam.ta10.selenium.project.decorator.elements.Button;
import com.epam.ta10.selenium.project.decorator.elements.Label;
import com.epam.ta10.selenium.project.decorator.elements.TextInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPersonalPage extends AbstractPage {

  @FindBy(xpath = "//div[@class='z0']/div[@role='button']")
  private Button createMessage;

  @FindBy(xpath = "//textarea[@name='to']")
  private TextInput messageRecipient;

  @FindBy(xpath = "//input[@name='subjectbox']")
  private TextInput messageTheme;

  @FindBy(xpath = "//div[@role='textbox']")
  private TextInput messageText;

  @FindBy(xpath = "//img[@class='Ha']")
  private WebElement saveAsDraft;

  @FindBy(xpath = "//div[contains(@data-tooltip, 'Enter')]")
  private Button sendMessage;

  @FindBy(xpath = "//a[contains(@href, '#drafts')]/parent::*")
  private WebElement drafts;

  @FindBy(xpath = "//div[@role='main']//tr[1]//div[@class='yW' and (//a[contains(@href, '#drafts') and @tabindex='0'])]")
  private WebElement newestDraft;

  @FindBy(xpath = "//a[contains(@href,'#drafts')]/parent::*/following-sibling::div[@class='bsU']")
  private WebElement draftsNumber;

  @FindBy(xpath = "//a[contains(@href, 'sent')]")
  private WebElement sentMessages;

  @FindBy(xpath = "(//div[@role='main']//div[@class='yW' and (//a[contains(@href, '#sent') and @tabindex='0'])]/span)[1]")
  private Label lastSentMessageRecipient;

  @FindBy(xpath = "(//div[@role='main']//span[@class='bog' and (//a[contains(@href, '#sent') and @tabindex='0'])])[1]")
  private Label lastSentMessageTheme;

  @FindBy(xpath = "(//div[@role='main']//span[@class='y2' and (//a[contains(@href, '#sent') and @tabindex='0'])])[1]")
  private Label lastSentMessageText;

  @FindBy(xpath = "(//div[@role='main']//span[@class='Zt' and (//a[contains(@href, '#sent') and @tabindex='0'])])[1]")
  private Label lastSentMessageTextInnerTechnicalLabel;

  public Button getCreateMessage() {
    return createMessage;
  }

  public TextInput getMessageRecipient() {
    return messageRecipient;
  }

  public TextInput getMessageTheme() {
    return messageTheme;
  }

  public TextInput getMessageText() {
    return messageText;
  }

  public WebElement getSaveAsDraft() {
    return saveAsDraft;
  }

  public Button getSendMessage() {
    return sendMessage;
  }

  public WebElement getDrafts() {
    return drafts;
  }

  public WebElement getNewestDraft() {
    return newestDraft;
  }

  public WebElement getDraftsNumber() {
    return draftsNumber;
  }

  public WebElement getSentMessages() {
    return sentMessages;
  }

  public Label getLastSentMessageRecipient() {
    return lastSentMessageRecipient;
  }

  public Label getLastSentMessageTheme() {
    return lastSentMessageTheme;
  }

  public Label getLastSentMessageText() {
    return lastSentMessageText;
  }

  public Label getLastSentMessageTextInnerTechnicalLabel() {
    return lastSentMessageTextInnerTechnicalLabel;
  }
}
