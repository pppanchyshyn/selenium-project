package com.epam.ta10.selenium.project.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyProvider {

  public final static Properties PROPERTIES;

  static {
    PROPERTIES = new Properties();
    Properties credentialsProperties = new Properties();
    loadProperties(PROPERTIES, "src/main/resources/path.PROPERTIES");
    loadProperties(credentialsProperties, "src/main/resources/credentials.PROPERTIES");
    PROPERTIES.putAll(credentialsProperties);
  }

  private PropertyProvider() {
  }

  private static void loadProperties(Properties properties, String path) {
    try {
      properties.load(new FileInputStream(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
