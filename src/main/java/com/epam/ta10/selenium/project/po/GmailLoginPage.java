package com.epam.ta10.selenium.project.po;

import com.epam.ta10.selenium.project.decorator.elements.Button;
import com.epam.ta10.selenium.project.decorator.elements.TextInput;
import com.epam.ta10.selenium.project.utils.DriverProvider;
import com.epam.ta10.selenium.project.utils.PropertyProvider;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage {

  @FindBy(id = "identifierId")
  private TextInput login;

  @FindBy(xpath = "//div[@id='identifierNext']//child::span[text()]")
  private Button next;

  public GmailLoginPage(){
    super();
    DriverProvider.getDriver().get(PropertyProvider.PROPERTIES.getProperty("gmailLoinPage"));
  }

  public TextInput getLogin() {
    return login;
  }

  public Button getNext() {
    return next;
  }
}
