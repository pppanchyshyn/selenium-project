package com.epam.ta10.selenium.project;

import com.epam.ta10.selenium.project.bo.GmailAuthenticationBO;
import com.epam.ta10.selenium.project.bo.GmailPersonalPageBO;
import com.epam.ta10.selenium.project.model.User;
import com.epam.ta10.selenium.project.utils.DriverProvider;
import java.util.Iterator;
import java.util.stream.Stream;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestSendingEmailFromDrafts {

  @BeforeClass()
  public void setUp() {
    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
  }

  @DataProvider(parallel = true)
  public Iterator<Object[]> users() {
    return Stream.of(
        new Object[]{Const.USER1},
        new Object[]{Const.USER2},
        new Object[]{Const.USER3},
        new Object[]{Const.USER4},
        new Object[]{Const.USER5}).iterator();
  }

  @Test(dataProvider = "users")
  public void checkUserAbilityToSendEmailFromDraft(User user) {
    GmailAuthenticationBO gmailAuthenticationBO = new GmailAuthenticationBO();
    gmailAuthenticationBO.login(user);
    GmailPersonalPageBO gmailPersonalPageBO = new GmailPersonalPageBO();
    int draftsNumberBeforeCreatingNewOne = gmailPersonalPageBO.getDraftsNumber();
    gmailPersonalPageBO.createMessage(Const.MESSAGE);
    gmailPersonalPageBO.saveMessageAsDraft();
    gmailPersonalPageBO.moveToDrafts();
    int draftsNumberAfterCreatingNewOne = gmailPersonalPageBO.getDraftsNumber();
    Assert.assertEquals(draftsNumberAfterCreatingNewOne, draftsNumberBeforeCreatingNewOne + 1,
        "The number of drafts after creation of a new one should be one more than before");
    gmailPersonalPageBO.sendMessageFromDrafts();
    gmailPersonalPageBO.moveToSentMessages();
    Assert.assertTrue(gmailPersonalPageBO.isLastSentMessageEqualsToCreatedMessage(Const.MESSAGE),
        "Last sent message is not equals to created message");
  }

  @AfterMethod
  public void closeWebDriver() {
    DriverProvider.close();
  }
}
